dnl Process this file with autoconf to produce a configure script.

AC_PREREQ(2.52)

AC_INIT(marmot, 0.0.1,)
AC_CANONICAL_SYSTEM
AC_CONFIG_SRCDIR(.)
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)

MAJOR_VERSION=1
AC_SUBST(MAJOR_VERSION)

AM_MAINTAINER_MODE
AM_CONFIG_HEADER(config.h)

AM_PROG_LIBTOOL

AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_STDC
AC_HEADER_STDC
AC_PROG_INSTALL
AC_PROG_MAKE_SET

dnl
dnl Start of pkg-config checks
dnl
PKG_CHECK_MODULES(DAEMON, glib-2.0 gthread-2.0 dbus-glib-1.0)
AC_SUBST(DAEMON_CFLAGS)
AC_SUBST(DAEMON_LIBS)

PKG_CHECK_MODULES(LIBMARMOT, glib-2.0 gthread-2.0 dbus-glib-1.0)
AC_SUBST(LIBMARMOT_LIBS)
AC_SUBST(LIBMARMOT_CFLAGS)

PKG_CHECK_MODULES(TEST, glib-2.0 gthread-2.0)
AC_SUBST(TEST_LIBS)
AC_SUBST(TEST_CFLAGS)

dnl AC_CHECK_GTHREAD_SUPPORTED

dnl Turn on the additional warnings last, so -Werror doesn't affect other tests.

AC_ARG_ENABLE(more-warnings,
[  --enable-more-warnings  Maximum compiler warnings],
set_more_warnings="$enableval",[
if test -f $srcdir/CVSVERSION; then
	is_cvs_version=true
	set_more_warnings=yes
else
	set_more_warnings=no
fi
])
AC_MSG_CHECKING(for more warnings, including -Werror)
if test "$GCC" = "yes" -a "$set_more_warnings" != "no"; then
	AC_MSG_RESULT(yes)
	CFLAGS="\
	-Wall \
	-Wchar-subscripts -Wmissing-declarations -Wmissing-prototypes \
	-Wnested-externs -Wpointer-arith \
	-Wcast-align -Wsign-compare \
	-Werror \
	$CFLAGS"

	for option in -Wsign-promo -Wno-sign-compare; do
		SAVE_CFLAGS="$CFLAGS"
		CFLAGS="$option $CFLAGS"
		AC_MSG_CHECKING([whether gcc understands $option])
		AC_TRY_COMPILE([], [],
			has_option=yes,
			has_option=no,)
		if test $has_option = no; then
			CFLAGS="$SAVE_CFLAGS"
		fi
		AC_MSG_RESULT($has_option)
		unset has_option
		unset SAVE_CFLAGS
	done
	unset option
else
	AC_MSG_RESULT(no)
fi

AC_ARG_ENABLE(debug,
[  --enable-debug  Enable debugging messages],
AC_DEFINE([MARMOT_DEBUG], 1, [Enable debugging messages]),)

AC_ARG_ENABLE(docs,
[  --enable-docs   Build documentation (requires Doxygen)],
[case "${enableval}" in
  yes) build_docs=yes ;;
  no)  build_docs=no ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --enable-docs) ;;
esac],[build_docs=yes])
AM_CONDITIONAL(BUILD_DOCS, test x$build_docs = xyes)

AC_ARG_ENABLE(kernel,
[  --disable-kernel Use polling regardless of what kernel-level systems are available],
[case "${enableval}" in
  yes) os=${target_os} ;;
  no)  os="BogusOS" ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --disable-kernel) ;;
esac],[os=${target_os}])

dnl check what OS we're on
AM_CONDITIONAL(HAVE_LINUX, test x$os = xlinux-gnu)

if test x$os = xlinux-gnu; then
	AC_DEFINE([HAVE_LINUX],[],[Whether we are using linux or not])
	backend="dnotify"
else
	backend="polling"
fi

dnl check for flavours of varargs macros (test from GLib)
AC_MSG_CHECKING(for ISO C99 varargs macros in C)
AC_TRY_COMPILE([],[
int a(int p1, int p2, int p3);
#define call_a(...) a(1,__VA_ARGS__)
call_a(2,3);
],dbus_have_iso_c_varargs=yes,dbus_have_iso_c_varargs=no)
AC_MSG_RESULT($dbus_have_iso_c_varargs)
                                                                                
AC_MSG_CHECKING(for GNUC varargs macros)
AC_TRY_COMPILE([],[
int a(int p1, int p2, int p3);
#define call_a(params...) a(1,params)
call_a(2,3);
],dbus_have_gnuc_varargs=yes,dbus_have_gnuc_varargs=no)
AC_MSG_RESULT($dbus_have_gnuc_varargs)
                                                                                
dnl Output varargs tests
if test x$dbus_have_iso_c_varargs = xyes; then
    AC_DEFINE(HAVE_ISO_VARARGS,1,[Have ISO C99 varargs macros])
fi
if test x$dbus_have_gnuc_varargs = xyes; then
    AC_DEFINE(HAVE_GNUC_VARARGS,1,[Have GNU-style varargs macros])
fi


dnl ==========================================================================

AC_OUTPUT([
Makefile
Doxyfile
daemon/Makefile
common/Makefile
libmarmot/Makefile
tests/Makefile
marmot.pc
daemon/org.gnome.Marmot.service
])

echo "
marmot-$VERSION:
                                                                                
        prefix:                   ${prefix}
        source code location:     ${srcdir}
        compiler:                 ${CC}
                                                                                
	backend:                  ${backend}
	build documentation:      ${build_docs}
"
