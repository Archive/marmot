#ifndef __MARMOT_EVENTS_H__
#define __MARMOT_EVENTS_H__

typedef enum {
	MARMOT_EVENT_CHANGED = 1 << 4,
	MARMOT_EVENT_CREATED = 1 << 5,
	MARMOT_EVENT_DELETED = 1 << 6,
	MARMOT_EVENT_MOVED = 1 << 7,
	MARMOT_EVENT_EXISTS = 1 << 8
} MarmotEventType;


#endif /* __MARMOT_EVENTS_H__ */
