

#ifndef __MD_DBUS_H__
#define __MD_DBUS_H__

#define MARMOT_SERVICE_NAME "org.freedesktop.Marmot"
#define MARMOT_MESSAGE_SUBSCRIBE_FILE MARMOT_SERVICE_NAME ".SubscribeFile"
#define MARMOT_MESSAGE_SUBSCRIBE_DIRECTORY MARMOT_SERVICE_NAME ".SubscribeDirectory"
#define MARMOT_MESSAGE_CANCEL MARMOT_SERVICE_NAME ".CancelSubscription"
#define MARMOT_MESSAGE_EVENT MARMOT_SERVICE_NAME ".Event"

#endif /* __MD_DBUS_H__ */
