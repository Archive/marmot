

#ifndef __MD_DEBUG_H__
#define __MD_DEBUG_H__

#include <config.h>

/* Supposedly, __FUNCTION_ is not defined in Irix */
#ifndef __GNUC__
#define __FUNCTION__   ""
#endif


void md_debug_real (char *file, int line, char* function, char* format, ...);

#ifdef MARMOT_DEBUG
/* Supposedly, __FUNCTION_ is not defined in Irix */
#  ifndef __GNUC__
#    define __FUNCTION__   ""
#  endif
#  define DEBUG_INFO __FILE__, __LINE__, __FUNCTION__
#  define md_debug md_debug_real
#else
#  ifdef HAVE_ISO_VARARGS
#    define md_debug(...)
#  elif defined (HAVE_GNUC_VARARGS)
#    define md_debug(format...)
#  else
#    error "This compiler does not support varargs macros and thus debug messages can't be disabled meaningfully"
#  endif
#endif /* !MARMOT_DEBUG */


#endif /* __MD_DEBUG_H__ */
