

#ifndef __MD_EVENT_H__
#define __MD_EVENT_H__

#include "marmot-events.h"

const char *md_event_to_string (MarmotEventType event);

#endif /* __MD_EVENT_H__ */
