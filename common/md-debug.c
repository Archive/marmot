/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>

#include <glib.h>
#include "md-debug.h"

void
md_debug_real (char *file, int line, char* function, char* format, ...)
{
	static gboolean debug_enabled = TRUE;
	static gboolean debug_initted = FALSE;
	va_list args;
	char *msg;

	g_return_if_fail (format != NULL);

	if (!debug_enabled)
		return;

	if (!debug_initted) {
		debug_enabled = g_getenv ("MARMOT_DEBUG") != NULL;
		debug_initted = TRUE;

		if (!debug_enabled)
			return;
	}

	va_start (args, format);
	msg = g_strdup_vprintf (format, args);
	va_end (args);

	g_print ("%s:%d (%s) %s\n", file, line, function, msg);
	g_free (msg);
}
