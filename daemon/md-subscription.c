/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <config.h>
#include <sys/types.h>
#include <regex.h>
#include <glib.h>
#include <marmot-events.h>
#include "md-listener.h"
#include "md-subscription.h"
#include "md-event.h"

struct _MdSubscription {
	char *path;
	int events;

	gboolean is_dir;
	gboolean recursive;
	gboolean cancelled;
	gboolean have_filter;
	regex_t reg;

	MdListener *listener;
};


/**
 * @defgroup MdSubscription MdSubscription
 * @ingroup Daemon
 * @brief MdSubscription API.
 *
 * A #MdSubscription represents a single monitoring request (or "subscription").
 *
 * @{
 */

/**
 * Creates a new MdSubscription
 *
 * @param path the path to be monitored
 * @param events the events that are accepted
 * @param filter a regular expression representing acceptable filenames.  For
 * example, ".*\\\.txt" would only allow events to occur on filenames ending in
 * ".txt".
 * @param recursive if it's a directory subscription, whether or not it should
 * be recursive
 * @param is_dir whether the subscription is for a directory or not
 * @returns the new MdSubscription
 */
MdSubscription *
md_subscription_new (const char *path,
		     int events,
		     const char *filter,
		     gboolean recursive,
		     gboolean is_dir)
{
	MdSubscription *sub;

	sub = g_new0 (MdSubscription, 1);
	sub->path = g_strdup (path);
	sub->events = events;

	/* everyone accepts this */
	md_subscription_set_event (sub, MARMOT_EVENT_EXISTS);
	
	if (filter) {
		if (regcomp (&sub->reg, filter, REG_EXTENDED|REG_NOSUB) == 0)
			sub->have_filter = TRUE;
	} else {
		sub->have_filter = FALSE;
	}

	sub->is_dir = is_dir;
	sub->recursive = recursive;

	return sub;
}

/**
 * Frees a MdSubscription
 *
 * @param sub the MdSubscription
 */
void
md_subscription_free (MdSubscription *sub)
{
	if (sub->have_filter)
		regfree (&sub->reg);

	g_free (sub->path);
	g_free (sub);
}

/**
 * Tells if a MdSubscription is for a directory or not
 *
 * @param sub the MdSubscription
 * @returns TRUE if the subscription is for a directory, FALSE otherwise
 */
gboolean
md_subscription_is_dir (MdSubscription *sub)
{
	return sub->is_dir;
}

/**
 * Tells is a MdSubscription is recursive or not
 *
 * @param sub the MdSubscription
 * @returns TRUE is the MdSubscription is recursive, FALSE otherwise
 */
gboolean
md_subscription_is_recursive (MdSubscription *sub)
{
	return sub->recursive;
}

/**
 * Gets the path for a MdSubscription
 *
 * @param sub the MdSubscription
 * @returns The path being monitored.  It should not be freed.
 */
G_CONST_RETURN char *
md_subscription_get_path (MdSubscription *sub)
{
	return sub->path;
}

/**
 * Gets the MdListener which owns this MdSubscription
 *
 * @param sub the MdSubscription
 * @returns the MdListener, or NULL
 */
MdListener *
md_subscription_get_listener (MdSubscription *sub)
{
	return sub->listener;
}

/**
 * Sets the MdListener which is owned by this MdSubscription
 *
 * @param sub the MdSubscription
 * @param listener the MdListener
 */
void
md_subscription_set_listener (MdSubscription *sub,
			      MdListener     *listener)
{
	sub->listener = listener;
}

/**
 * Set the events this MdSubscription is interested in
 *
 * @param sub the MdSubscription
 * @param event an ORed combination of the events desired
 */
void
md_subscription_set_event (MdSubscription *sub,
			   int             event)
{
	sub->events |= event;
}

/**
 * Removes an event from the set of acceptable events
 *
 * @param sub the MdSubscription
 * @param event the event to remove
 */
void
md_subscription_unset_event (MdSubscription *sub,
			     int             event)
{
	sub->events &= ~event;
}

/**
 *  
 * @param sub the MdSubscription
 * @param event the event to test for
 * @returns Whether or not this subscription accepts a given event
 */
gboolean
md_subscription_has_event (MdSubscription *sub,
			   int             event)
{
	return sub->events & event;
}

/**
 * Mark this MdSubscription as cancelled
 *
 * @param sub the MdSubscription
 */
void
md_subscription_cancel (MdSubscription *sub)
{
	sub->cancelled = TRUE;
}

/**
 * Checks if the MdSubscription is cancelled or not
 *
 * @param sub the MdSubscription
 * @returns TRUE if the MdSubscription is cancelled, FALSE otherwise
 */
gboolean
md_subscription_is_cancelled (MdSubscription *sub)
{
	return sub->cancelled == TRUE;
}

/**
 * Checks if a given path/event combination is accepted by this MdSubscription
 *
 * @param sub the MdSubscription
 * @param name file name (just the base name, not the complete path)
 * @param event the event
 * @returns TRUE if the combination is accepted, FALSE otherwise
 */
gboolean
md_subscription_wants_event (MdSubscription    *sub,
			     const char        *name,
			     MarmotEventType  event)
{
	if (sub->cancelled)
		return FALSE;

	if (!md_subscription_has_event (sub, event)) {
		return FALSE;
	}

	if (sub->have_filter) {
		gboolean ret;
		char *base = g_path_get_basename (name);

		ret = regexec (&sub->reg, base, 0, NULL, 0) == 0;

		g_free (base);

		return ret;
	}

	return TRUE;
}

/** @} */
