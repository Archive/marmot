
#ifndef __MD_LISTENER_H__
#define __MD_LISTENER_H__

#include <glib.h>

G_BEGIN_DECLS

typedef struct _MdListener MdListener;

typedef struct _MdSubscription MdSubscription;


MdListener   *md_listener_new                   (const char *service,
						 const char *id);

void          md_listener_free                  (MdListener *listener);

const char   *md_listener_get_service           (MdListener *listener);
const char   *md_listener_get_id                (MdListener *listener);

void          md_listener_add_subscription      (MdListener     *listener,
						 MdSubscription *sub);

gboolean      md_listener_remove_subscription   (MdListener     *listener,
						 MdSubscription *sub);

MdSubscription *md_listener_get_subscription    (MdListener *listener,
						 const char *path);

GList        *md_listener_get_subscriptions     (MdListener *listener);

gboolean      md_listener_is_subscribed         (MdListener *listener,
						 const char *path); 

G_END_DECLS

#endif /* __MD_LISTENER_H__ */
