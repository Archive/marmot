
#ifndef __MD_SUBSCRIPTION_H__
#define __MD_SUBSCRIPTION_H__

#include <glib.h>
#include <marmot-events.h>
#include "md-listener.h"

G_BEGIN_DECLS

MdSubscription      *md_subscription_new          (const char *path,
						   int         events,
						   const char *filter,
						   gboolean    recursive,
						   gboolean    is_dir);

void                 md_subscription_free         (MdSubscription *sub);

gboolean             md_subscription_is_dir       (MdSubscription *sub);

gboolean             md_subscription_is_recursive (MdSubscription *sub);

G_CONST_RETURN char *md_subscription_get_path     (MdSubscription *sub);

MdListener          *md_subscription_get_listener (MdSubscription *sub);

void                 md_subscription_set_listener (MdSubscription *sub,
						   MdListener     *listener);

void                 md_subscription_set_event    (MdSubscription *sub,
						   int             event);
void                 md_subscription_unset_event  (MdSubscription *sub,
						   int             event);
gboolean             md_subscription_has_event    (MdSubscription *sub,
						   int             event);

void                 md_subscription_cancel       (MdSubscription *sub);
gboolean             md_subscription_is_cancelled (MdSubscription *sub);

gboolean             md_subscription_wants_event  (MdSubscription    *sub,
						   const char        *name,
						   MarmotEventType  event);
G_END_DECLS

#endif /* __MD_SUBSCRIPTION_H__ */
