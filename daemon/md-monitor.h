
#ifndef __MD_MONITOR_H__
#define __MD_MONITOR_H__

#include <config.h>

#ifdef HAVE_LINUX
#include "md-poll.h"
#include "md-dnotify.h"
#define md_monitor_init md_dnotify_init
#define md_monitor_add_subscription md_dnotify_add_subscription
#define md_monitor_remove_subscription md_dnotify_remove_subscription
#define md_monitor_remove_all_for md_dnotify_remove_all_for
#else
#include "md-poll.h"
#define md_monitor_init md_poll_init
#define md_monitor_add_subscription md_poll_add_subscription
#define md_monitor_remove_subscription md_poll_remove_subscription
#define md_monitor_remove_all_for md_poll_remove_all_for
#endif

#endif /* __MD_MONITOR_H__ */
