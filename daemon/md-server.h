
#ifndef __MD_SERVER_H__
#define __MD_SERVER_H__

#include <glib.h>
#include "marmot-events.h"

G_BEGIN_DECLS

gboolean        md_server_init                  (void);
void            md_server_deinit                (void);
int             md_server_num_listeners         (void);
void            md_server_emit_event            (const char        *path,
						 MarmotEventType  event,
						 GList             *subs);


G_END_DECLS

#endif /* __MD_SERVER_H__ */
