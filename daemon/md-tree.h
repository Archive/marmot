
#ifndef __MD_TREE_H__
#define __MD_TREE_H__

#include <glib.h>
#include "md-node.h"

G_BEGIN_DECLS

typedef gboolean (*MdTreeForeachFunc) (MdNode *node);

typedef struct _MdTree MdTree;

MdTree        *md_tree_new                   (void);

void           md_tree_free                  (MdTree *tree);

gboolean       md_tree_add                   (MdTree *tree,
					      MdNode *parent,
					      MdNode *child);

gboolean       md_tree_remove                (MdTree *tree,
					      MdNode *node);

MdNode        *md_tree_add_at_path           (MdTree        *tree,
					      const char    *path,
					      gboolean       is_dir);

MdNode        *md_tree_get_at_path           (MdTree        *tree,
					      const char    *path);

/*
GList         *md_tree_get_all_above         (MdTree *tree,
					      MdNode *node);

gboolean       md_tree_has_parent_subscription (MdTree *tree, MdNode *node);
*/

GList         *md_tree_find_subscriptions    (MdTree        *tree,
					      MdListener    *listener);

void           md_tree_foreach_directory     (MdTree            *tree,
					      MdTreeForeachFunc  func);

void           md_tree_foreach_file          (MdTree            *tree,
					      MdNode            *dir,
					      MdTreeForeachFunc  func);

GList         *md_tree_get_directories       (MdTree            *tree,
					      MdNode            *root);

GList         *md_tree_get_files             (MdTree            *tree,
					      MdNode            *dir);

GList         *md_tree_get_children          (MdTree            *tree,
					      MdNode            *dir);

gboolean       md_tree_has_children          (MdTree            *tree,
					      MdNode            *node);

void           md_tree_dump                  (MdTree *tree, MdNode *node);

guint          md_tree_get_size              (MdTree *tree);

G_END_DECLS

#endif /* __MD_TREE_H__ */
