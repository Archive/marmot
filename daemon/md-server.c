/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <config.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <glib.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus.h>
#include "md-main.h"
#include "md-server.h"
#include "md-listener.h"
#include "md-monitor.h"
#include "md-dbus.h"
#include "md-debug.h"
#include "md-event.h"

/**
 * @defgroup Daemon Daemon
 *
 */

/**
 * @defgroup Backends Backends
 * @ingroup Daemon
 *
 * One of the goals for Marmot is providing a uniform and consistent
 * monitoring solution, which works even across different platforms.  Different
 * platforms have different kernel-level monitoring systems available (or
 * none at all).  A "backend" simply takes advantage of the services available
 * on a given platform and makes them work with the rest of Marmot.
 * 
 *
 */

static DBusConnection *dbus_conn = NULL;
static GHashTable     *listeners = NULL;

static DBusHandlerResult
md_server_message_handler (DBusMessageHandler *handler,
			   DBusConnection     *connection,
			   DBusMessage        *message,
			   void               *user_data)
{
	MdListener *listener;
	MdSubscription *sub;
	const char *sender;
	const char *sender_id;
	const char *path;
	int events;
	gboolean recursive;
	const char *filter;
	DBusMessageIter iter;

	md_debug (DEBUG_INFO, "Got a message: %s",
		  dbus_message_get_name (message));

	dbus_message_iter_init (message, &iter);

	sender = dbus_message_get_sender (message);
	dbus_message_iter_get_args (&iter, NULL,
				    DBUS_TYPE_STRING, &sender_id, 0);
	dbus_message_iter_next (&iter);

	listener = g_hash_table_lookup (listeners, sender_id);

	if (listener == NULL) {
		listener = md_listener_new (sender, sender_id);
				
		g_hash_table_insert (listeners,
				g_strdup (sender_id),
				listener);
	}

	if (dbus_message_has_name (message, MARMOT_MESSAGE_SUBSCRIBE_FILE)) {
		dbus_message_iter_get_args (&iter, NULL,
					    DBUS_TYPE_STRING, &path,
					    DBUS_TYPE_UINT32, &events, 0);

		if (!md_listener_is_subscribed (listener, path)) {
			sub = md_subscription_new (path, events, NULL,
						   FALSE, FALSE);
			md_subscription_set_listener (sub, listener);
			md_monitor_add_subscription (sub);
		}
	} else if (dbus_message_has_name (message,
				 MARMOT_MESSAGE_SUBSCRIBE_DIRECTORY)) {

		dbus_message_iter_get_args (&iter, NULL,
				       DBUS_TYPE_STRING, &path,
				       DBUS_TYPE_UINT32, &events,
				       DBUS_TYPE_UINT32, &recursive,
				       DBUS_TYPE_STRING, &filter, 0);

		if (!md_listener_is_subscribed (listener, path)) {
			sub = md_subscription_new (path, events, filter,
						   recursive, TRUE);
			md_subscription_set_listener (sub, listener);
			md_monitor_add_subscription (sub);
		}
	} else if (dbus_message_has_name (message, MARMOT_MESSAGE_CANCEL)) {
		MdSubscription *sub;
		const char *path;

		dbus_message_iter_get_args (&iter, NULL,
					    DBUS_TYPE_STRING, &path, 0);

		sub = md_listener_get_subscription (listener, path);
		if (sub) {
			md_monitor_remove_subscription (sub);
			//md_subscription_free (sub);
		}
	}

	return DBUS_HANDLER_RESULT_ALLOW_MORE_HANDLERS;
}

static gboolean
service_deleted_predicate (char *key, MdListener *listener, const char *service)
{
	if (strcmp (md_listener_get_service (listener), service) == 0) {
		md_monitor_remove_all_for (listener);
		return TRUE;
	}

	return FALSE;
}

static DBusHandlerResult
md_server_service_deleted_handler (DBusMessageHandler *handler,
				   DBusConnection     *connection,
				   DBusMessage        *message,
				   void               *user_data)
{
	char       *who;

	dbus_message_get_args (message, NULL, DBUS_TYPE_STRING, &who, 0);
	
	md_debug (DEBUG_INFO, "%s went away", who);

	g_hash_table_foreach_remove (listeners,
				     (GHRFunc)service_deleted_predicate,
				     who);

	return DBUS_HANDLER_RESULT_ALLOW_MORE_HANDLERS;
}

gboolean
md_server_init (void)
{
	DBusMessageHandler *handler;
	DBusError           error;
	int                 svc_reply;
	const char         *messages[] = { MARMOT_MESSAGE_SUBSCRIBE_FILE,
					   MARMOT_MESSAGE_SUBSCRIBE_DIRECTORY,
					   MARMOT_MESSAGE_CANCEL };
	const char         *delete_messages[] = { DBUS_MESSAGE_SERVICE_LOST,
						  DBUS_MESSAGE_SERVICE_DELETED };

	g_return_val_if_fail (md_monitor_init () == TRUE, FALSE);
	dbus_error_init (&error);
	
	listeners = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
					   (GDestroyNotify)md_listener_free);

	dbus_conn = dbus_bus_get (DBUS_BUS_ACTIVATION, &error);

	if (!dbus_conn) {
		g_warning ("Could not connect to DBus server: %s", error.message);
		return FALSE;
	}

	dbus_connection_setup_with_g_main (dbus_conn, NULL);

	svc_reply = dbus_bus_acquire_service (dbus_conn,
					MARMOT_SERVICE_NAME,
					0, NULL);

	if (svc_reply == -1) {
		g_warning ("Could not acquire the marmot service");
		return FALSE;
	}


	if (svc_reply != DBUS_SERVICE_REPLY_PRIMARY_OWNER) {
		g_warning ("Someone else already owns the marmot daemon service.");
		g_warning ("Reply was: %d", svc_reply);
		return FALSE;
	} else {
		md_debug (DEBUG_INFO, "We own the marmot service");
	}

	handler = dbus_message_handler_new (md_server_message_handler,
					    NULL, NULL);
	if (!dbus_connection_register_handler (dbus_conn, handler, messages,
					       G_N_ELEMENTS (messages))) {
		g_warning ("Could not register message handler");
		return FALSE;
	}

	handler = dbus_message_handler_new (md_server_service_deleted_handler,
					    NULL, NULL);
	if (!dbus_connection_register_handler (dbus_conn, handler,
					       delete_messages,
					       G_N_ELEMENTS (delete_messages))) {
		g_warning ("Could not register message handler");
		return FALSE;
	}

	md_debug (DEBUG_INFO, "Marmot daemon up and running");
	
	return TRUE;
}

void
md_server_deinit (void)
{
	
}

static gboolean
md_server_send_idler (DBusMessage *msg)
{
	dbus_connection_send (dbus_conn, msg, NULL);
	dbus_message_unref (msg);

	return FALSE;
}

void
md_server_emit_event (const char *path,
		      MarmotEventType event,
		      GList *subs)
{
	DBusMessage *msg;
	GList *l;

	for (l = subs; l; l = l->next) {
		MdSubscription *sub = l->data;

		if (!md_subscription_wants_event (sub, path, event))
			continue;

		md_debug (DEBUG_INFO, "%s : %s : %s",
			  md_listener_get_id (md_subscription_get_listener (sub)),
			  path,
			  md_event_to_string (event));

		msg = dbus_message_new (MARMOT_MESSAGE_EVENT,
					md_listener_get_service (md_subscription_get_listener (sub)));

		dbus_message_append_args (msg,
			DBUS_TYPE_STRING,
			md_listener_get_id (md_subscription_get_listener (sub)),
			DBUS_TYPE_STRING, md_subscription_get_path (sub),
			DBUS_TYPE_STRING, path,
			DBUS_TYPE_UINT32, event, 0);

		/* there seem to be some thread-safety issues in dbus,
		 * so we will send the message in an idle handler to avoid
		 * those for now
		 */
		g_idle_add ((GSourceFunc)md_server_send_idler, msg);
	}
}

int
md_server_num_listeners (void)
{
	return g_hash_table_size (listeners);
}
