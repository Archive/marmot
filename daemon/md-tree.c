/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include "md-tree.h"
#include "md-node.h"


#define NODE_DATA(x) (MdNode *)x->data

struct _MdTree {
	GNode *root; /* The root of the tree, which is always a node
		      * representing /
		      */
	GHashTable *node_hash; /* a hash table that maps path->node */

	GMutex *lock;
};

typedef struct {
	MdListener *listener;
	GList      *list;
} SubSearchData;

static GNode *
new_node (MdNode *node)
{
	GNode *gnode;

	gnode = g_node_new (node);
	node->node = gnode;

	return gnode;
}


/**
 * @defgroup MdTree MdTree
 * @ingroup Daemon
 * @brief MdTree API
 *
 * @{
 */

/**
 * Creates a new MdTree object.
 * 
 * The MdTree is useful for attaching data to files/directories in
 * a filesystem.
 *
 * @returns a new #MdTree
 */
MdTree *
md_tree_new (void)
{
	MdTree *tree;

	tree = g_new0 (MdTree, 1);
	tree->root = new_node (md_node_new ("/", NULL, TRUE));
	tree->node_hash = g_hash_table_new_full (g_str_hash, g_str_equal,
						 g_free, NULL);
	tree->lock = g_mutex_new ();

	return tree;
}

/**
 * Destroys a previously created tree.
 *
 * @param tree the tree
 */
void
md_tree_free (MdTree *tree)
{
	g_node_destroy (tree->root);
	g_hash_table_destroy (tree->node_hash);
	g_mutex_free (tree->lock);

	g_free (tree);
}

/**
 * Adds a node to the tree
 *
 * @param tree the tree
 * @param parent the parent of the node to be inserted
 * @param child the node to insert
 * @returns TRUE if the node was added, FALSE otherwise
 */
gboolean
md_tree_add (MdTree *tree,
	     MdNode *parent,
	     MdNode *child)
{
	GNode *node;

	g_mutex_lock (tree->lock);

	if (g_hash_table_lookup (tree->node_hash, md_node_get_path (child)))
		return FALSE;

	/*
	g_message ("Adding child: %s to %s, %d : %s",
		   md_node_get_path (child),
		   md_node_get_path (parent),
		   g_node_n_children (parent->node),
		   md_node_is_dir (child) ? "directory" : "file");
	*/
	node = new_node (child);
	g_node_append (parent->node, node);
	g_hash_table_insert (tree->node_hash,
			     g_strdup (md_node_get_path (child)),
			     node);

	g_mutex_unlock (tree->lock);

	return TRUE;
}

/**
 * Removes a node from the tree
 *
 * @param tree the tree
 * @param node the node to remove
 * @returns TRUE if the node was removed, FALSE otherwise
 */
gboolean
md_tree_remove (MdTree *tree,
		MdNode *node)
{
	gboolean ret = FALSE;

	g_mutex_lock (tree->lock);

	if (g_node_is_ancestor (tree->root, node->node)) {

		g_assert (g_node_n_children (node->node) == 0);

		g_hash_table_remove (tree->node_hash, md_node_get_path (node));
		g_node_unlink (node->node);
		g_node_destroy (node->node);
		md_node_free (node);
		ret = TRUE;
	}

	g_mutex_unlock (tree->lock);

	return ret;
}

/**
 * Gets a node given a filesystem path.
 *
 * @param tree the tree
 * @param path the path to lookup
 * @returns the #MdNode corresponding to the provided path, or NULL if not
 * found
 */
MdNode *
md_tree_get_at_path (MdTree     *tree,
		     const char *path)
{
	GNode *node;

	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (path != NULL, NULL);

	node = g_hash_table_lookup (tree->node_hash, path);
	if (node)
		return NODE_DATA (node);
	else
		return NULL;
}

/**
 * Adds a node given a filesystem path.
 *
 * This is like #md_tree_add, except all parents of the path
 * are created if they don't exist.  For instance, calling this function
 * on "/tmp/foo/bar/blah.txt" will create the directories "tmp", "foo",
 * and "bar" if they don't exist and insert "blah.txt" under "bar".
 *
 * @param tree the tree
 * @param path the path to add
 * @param is_dir TRUE if the node is supposed to be a directory
 * @returns the new #MdNode
 */
MdNode *
md_tree_add_at_path (MdTree     *tree,
		     const char *path,
		     gboolean    is_dir)
{
	MdNode *parent;
	MdNode *node;
	unsigned int i;
	char *path_cpy;

	g_return_val_if_fail (strlen (path) > 0, NULL);

	if ((node = md_tree_get_at_path (tree, path)) != NULL)
		return node;

	path_cpy = g_strdup (path);

	parent = NODE_DATA (tree->root);
	g_assert (parent != NULL);

	for (i = 1; i < strlen (path_cpy); i++) {
		MdNode *new_parent;

		if (path_cpy[i] == '/') {
			path_cpy[i] = '\0';
			new_parent = md_tree_get_at_path (tree, path_cpy);

			if (new_parent) {
				parent = new_parent;
			} else {
				new_parent = md_node_new (path_cpy, NULL,
							  TRUE);
				md_tree_add (tree, parent, new_parent);
				parent = new_parent;
			}

			path_cpy[i] = '/';
		}
	}

	node = md_node_new (path, NULL, is_dir);
	md_tree_add (tree, parent, node);

	g_free (path_cpy);

	return node;
}
		 

/*
GList *
md_tree_get_all_above (MdTree *tree, MdNode *node)
{
	GList *ret = NULL;
	GNode *gnode;

	for (gnode = node->node; gnode; gnode = gnode->parent) {
		ret = g_list_prepend (ret, NODE_DATA (gnode));
	}

	return ret;
}

gboolean
md_tree_has_parent_subscription (MdTree *tree, MdNode *node)
{
	GNode *gnode;

	if (md_node_get_subscriptions (NODE_DATA (node->node->parent)))
		return TRUE;

	for (gnode = node->node; gnode; gnode = gnode->parent) {
		GList *l;

		for (l = md_node_get_subscriptions (NODE_DATA (gnode)); l; l = l->next) {
			MdSubscription *sub = (MdSubscription *)l->data;

			if (md_subscription_is_recursive (sub))
				return TRUE;
		}
	}

	return FALSE;
}
*/

/*
static gboolean
md_tree_find_subs_traverse_func (GNode *node, gpointer user_data)
{
	SubSearchData *data = user_data;
	GList *l;

	for (l = md_node_get_subscriptions (NODE_DATA (node)); l; l = l->next) {
		MdSubscription *sub = (MdSubscription *)l->data;

		if (md_subscription_get_listener (sub) == data->listener)
			data->list = g_list_prepend (data->list, sub);
	}

	return FALSE;
}

GList *
md_tree_find_subscriptions (MdTree        *tree,
			    MdListener    *listener)
{
	GList *ret;
	SubSearchData *data;

	data = g_new0 (SubSearchData, 1);
	data->listener = listener;
	data->list = NULL;

	g_mutex_lock (tree->lock);
	g_node_traverse (tree->root, G_LEVEL_ORDER, G_TRAVERSE_ALL,
			 -1, md_tree_find_subs_traverse_func, data);
	g_mutex_unlock (tree->lock);

	ret = data->list;
	g_free (data);

	return ret;
}

static gboolean
md_tree_foreach_dir_traverse_func (GNode *node, gpointer user_data)
{
	MdTreeForeachFunc func = (MdTreeForeachFunc)user_data;

	if (md_node_is_dir (NODE_DATA (node)))
		return (*func)(NODE_DATA (node));
	else
		return FALSE;
}

void
md_tree_foreach_directory (MdTree            *tree,
			   MdTreeForeachFunc  func)
{
	g_node_traverse (tree->root, G_LEVEL_ORDER, G_TRAVERSE_ALL,
			 -1, md_tree_foreach_dir_traverse_func, func);
}

static void
md_tree_foreach_file_foreach_func (GNode *node, gpointer user_data)
{
	MdTreeForeachFunc func = (MdTreeForeachFunc)user_data;

	if (!md_node_is_dir (NODE_DATA (node)))
		(*func)(NODE_DATA (node));
}

void
md_tree_foreach_file (MdTree            *tree,
		      MdNode            *dir,
		      MdTreeForeachFunc  func)
{
	g_node_children_foreach (dir->node, G_TRAVERSE_ALL,
				 md_tree_foreach_file_foreach_func, func);

}
*/

static gboolean
md_tree_get_dir_traverse_func (GNode *node, gpointer user_data)
{
	GList **list = user_data;

	if (md_node_is_dir (NODE_DATA (node)))
		*list = g_list_prepend (*list, NODE_DATA (node));
		
		
	return FALSE;
}

/**
 * Gets all directories below a given node
 *
 * @param tree the tree
 * @param root where to start from, NULL if you want to start from the root
 * @returns a list of #MdNode
 */
GList *
md_tree_get_directories (MdTree *tree, MdNode *root)
{
	GList *list = NULL;

	g_node_traverse (root ? root->node : tree->root,
			 G_LEVEL_ORDER, G_TRAVERSE_ALL,
			 -1, md_tree_get_dir_traverse_func, &list);

	return list;
}

/**
 * Gets the immediate children below a given node
 *
 * @param tree the tree
 * @param root where to start from, NULL if you want to start from the root
 * @returns a list of #MdNode
 */
GList *
md_tree_get_children (MdTree *tree, MdNode *root)
{
	GList *list = NULL;
	GNode *node;
	unsigned int i;
	
	node = root ? root->node : tree->root;
	
	for (i = 0; i < g_node_n_children (node); i++) {
		list = g_list_prepend (list,
				NODE_DATA (g_node_nth_child (node, i)));
	}

	return list;
}

/**
 * Tells whether a given node has any children
 *
 * @param tree the tree
 * @param node the node
 * @returns TRUE if the node has children, FALSE otherwise
 */
gboolean
md_tree_has_children (MdTree *tree,
		      MdNode *node)
{
	return g_node_n_children (node->node) > 0;
}

/**
 * Gets the files immediately below a given node
 *
 * @param tree the tree
 * @param dir_node where to start from, NULL if you want to start from the root
 * @returns a list of #MdNode
 */
GList *
md_tree_get_files (MdTree *tree, MdNode *dir_node)
{
	GList *list = NULL;
	GNode *node;
	unsigned int i;
	
	node = dir_node ? dir_node->node : tree->root;
	
	for (i = 0; i < g_node_n_children (node); i++) {
		MdNode *child = NODE_DATA (g_node_nth_child (node, i));
		if (!md_node_is_dir (child))
			list = g_list_prepend (list, child);
	}

	return list;
}

static gboolean
md_tree_dump_traverse_func (GNode *node, gpointer data)
{
	g_print ("%s: %d listeners, %d children\n",
		 md_node_get_path (NODE_DATA (node)),
		 g_list_length ((GList *)md_node_get_subscriptions (NODE_DATA (node))),
		 g_node_n_children (node));

	return FALSE;
}

/**
 * Prints the contents of the tree
 *
 * @param tree the tree
 * @param node the node to start from, NULL if root
 */
void
md_tree_dump (MdTree *tree, MdNode *node)
{
	g_node_traverse (node ? node->node : tree->root, G_LEVEL_ORDER,
			 G_TRAVERSE_ALL,
			 -1, md_tree_dump_traverse_func, NULL);
}

/**
 * Gets the number of nodes in the tree
 *
 * @param tree the tree
 * @returns the size of the tree
 */
guint
md_tree_get_size (MdTree *tree)
{
	return g_hash_table_size (tree->node_hash);
}

/** @} */
