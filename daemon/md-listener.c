/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>

#include <string.h>
#include <glib.h>
#include "md-listener.h"
#include "md-subscription.h"

/* private struct representing a single listener */
struct _MdListener {
	char *service;
	char *id;
	GList *subs;
};

/**
 * @defgroup MdListener MdListener
 * @ingroup Daemon
 * @brief MdListener API.
 *
 * @{
 */

/**
 * Create a new #MdListener.
 *
 * @param service the D-Bus service the listener resides on
 * @param id the unique ID for this listener
 * @returns the new #MdListener
 */
MdListener *
md_listener_new (const char *service, const char *id)
{
	MdListener *listener;

	g_return_val_if_fail (service != NULL, NULL);
	g_return_val_if_fail (id != NULL, NULL);
	
	listener = g_new0 (MdListener, 1);
	listener->service = g_strdup (service);
	listener->id = g_strdup (id);
	listener->subs = NULL;

	return listener;
}

/**
 * Frees a previously created #MdListener
 *
 * @param listener the #MdListener to free
 */
void
md_listener_free (MdListener *listener)
{
	g_list_free (listener->subs);
	g_free (listener->service);
	g_free (listener);
}

/**
 * Gets the service associated with a #MdListener
 *
 * The result is owned by the #MdListener and should not be freed.
 *
 * @param listener the listener
 */
const char *
md_listener_get_service (MdListener *listener)
{
	return listener->service;
}

/**
 * Gets the unique ID associated with a #MdListener
 *
 * The result is owned by the #MdListener and should not be freed.
 *
 * @param listener the listener
 */
const char *
md_listener_get_id (MdListener *listener)
{
	return listener->id;
}

/**
 * Gets the subscription represented by the given path
 *
 * @param listener the listener
 * @param path a path to a file or directory
 * @returns a #MdSubscription, or NULL if it wasn't found
 */
MdSubscription *
md_listener_get_subscription (MdListener *listener, const char *path)
{
	GList *l;

	for (l = listener->subs; l; l = l->next) {
		MdSubscription *sub = l->data;

		if (strcmp (md_subscription_get_path (sub), path) == 0)
			return sub;
	}

	return NULL;
}

/**
 * Tells if a given #MdListener is subscribed to a file/directory
 *
 * @param listener the listener
 * @param path the path to check for
 */
gboolean
md_listener_is_subscribed (MdListener *listener, const char *path)
{
	return md_listener_get_subscription (listener, path) != NULL;
}

/**
 * Adds a subscription to the listener.
 *
 * @param listener the listener
 * @param sub the #MdSubscription to add
 */
void
md_listener_add_subscription (MdListener     *listener,
			      MdSubscription *sub)
{
	g_assert (sub != NULL);

	listener->subs = g_list_prepend (listener->subs, sub);
}

/**
 * Removes a subscription from the listener.
 *
 * @param listener the listener
 * @param sub the #MdSubscription to remove
 * @returns TRUE if the removal was successful, otherwise FALSE
 */
gboolean
md_listener_remove_subscription   (MdListener     *listener,
				   MdSubscription *sub)
{
	listener->subs = g_list_remove (listener->subs, sub);

	return TRUE;
}

/**
 * Get all subscriptions a given listener holds
 *
 * @param listener the listener
 * @returns a list of #MdSuscription, or NULL if the listener has no
 * subscriptions
 */
GList *
md_listener_get_subscriptions (MdListener *listener)
{
	return g_list_copy (listener->subs);
}

/** @} */
