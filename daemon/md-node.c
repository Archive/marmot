/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <config.h>
#include <string.h>
#include <glib.h>
#include "marmot-events.h"
#include "md-node.h"

/**
 * @defgroup MdNode MdNode
 * @ingroup Daemon
 * @brief MdNode API.
 *
 * A node represents a single file or directory.
 * 
 * @{
 */

/**
 * Create a new node
 *
 * @param path the path the node will represent
 * @param sub an initial MdSubscription for the node, could be NULL
 * @param is_dir whether the node is a directory or not
 * @returns the new node
 */
MdNode *
md_node_new (const char     *path,
	     MdSubscription *sub,
	     gboolean        is_dir)
{
	MdNode *node;

	node = g_new0 (MdNode, 1);

	node->path = g_strdup (path);
	if (sub)
		node->subs = g_list_append (NULL, sub);
	else
		node->subs = NULL;

	node->is_dir = is_dir;
	node->data = NULL;
	node->data_destroy = NULL;
	node->flags = 0;
	node->lock = g_mutex_new ();

	return node;
}

/**
 * Frees a node
 *
 * @param node the node
 */
void
md_node_free (MdNode *node)
{
	g_return_if_fail (node != NULL);

	g_assert (g_list_length (node->subs) == 0);

	if (node->data_destroy && node->data)
		(*node->data_destroy) (node->data);

	g_free (node->path);
	g_list_free (node->subs);
	g_mutex_free (node->lock);
	g_free (node);
}

/**
 * Retrieves the parent of a given node
 *
 * @param node the node
 * @returns the parent, or NULL
 */
MdNode *
md_node_parent (MdNode *node)
{
	MdNode *ret = NULL;

	/* md_node_lock (node); */

	if (node->node && node->node->parent)
		ret = (MdNode *)node->node->parent->data;

	/* md_node_unlock (node); */

	return ret;
}

/**
 * Checks whether a node is a directory or not
 *
 * @param node the node
 * @returns TRUE if the node is a directory, FALSE otherwise
 */
gboolean
md_node_is_dir (MdNode *node)
{
	return node->is_dir;
}

/**
 * Sets whether a node is a directory or not
 *
 * @param node the node
 * @param is_dir whether the node is a directory
 */
void
md_node_set_is_dir (MdNode *node, gboolean is_dir)
{
	node->is_dir = is_dir;
}

/**
 * Gets the path a given node represents
 *
 * @param node the node
 * @returns The path.  It should not be freed.
 */
G_CONST_RETURN char *
md_node_get_path (MdNode *node)
{
	return node->path;
}

/**
 * Returns a list of subscriptions attached to the node
 *
 * @param node the node
 * @returns a list of #MdSubscription, or NULL
 */
GList *
md_node_get_subscriptions (MdNode *node)
{
	return node->subs;
}

/**
 * Adds a subscription to a node
 *
 * @param node the node
 * @param sub the subscription
 */
gboolean
md_node_add_subscription (MdNode         *node,
			  MdSubscription *sub)
{
	/* md_node_lock (node); */

	if (!g_list_find (node->subs, sub))
		node->subs = g_list_prepend (node->subs, sub);

	/* md_node_unlock (node); */

	return TRUE;
}

/**
 * Removes a subscription from a node
 *
 * @param node the node
 * @param sub the subscription to remove
 * @returns TRUE if the subscription was removed, FALSE otherwise
 */
gboolean
md_node_remove_subscription (MdNode         *node,
			     MdSubscription *sub)
{
	/* md_node_lock (node); */
	node->subs = g_list_remove (node->subs, sub);
	/* md_node_unlock (node); */

	return TRUE;
}

/**
 * Copys subscriptions from one node to another
 *
 * @param src the source node
 * @param dest the destination node
 * @param filter a function which evaluates whether a subscription should be
 * copied or not.
 * @returns the number of subscriptions copied
 */
int
md_node_copy_subscriptions (MdNode         *src,
			    MdNode         *dest,
			    MdSubFilterFunc filter)
{
	GList *l;
	MdSubscription *sub;
	int i = 0;

	md_node_lock (src);
	for (l = md_node_get_subscriptions (src); l; l = l->next) {
		sub = (MdSubscription *)l->data;
		if (!filter || (filter && (*filter)(sub))) {
			md_node_add_subscription (dest, sub);
			i++;
		}
	}
	md_node_unlock (src);

	return i;
}
/**
 * Checks whether a node has a recursive subscription
 *
 * @param node the node
 * @returns TRUE if the node has a recursive subscription, FALSE otherwise
 */
gboolean
md_node_has_recursive_sub (MdNode *node)
{
	GList *l;

	/* md_node_lock (node); */

	for (l = md_node_get_subscriptions (node); l; l = l->next) {
		MdSubscription *sub = (MdSubscription *)l->data;

		if (md_subscription_is_recursive (sub))
			return TRUE;
	}

	/* md_node_unlock (node); */

	return FALSE;
}

/**
 * Attaches some arbitrary data to the node
 *
 * @param node the node
 * @param data a pointer to some data
 * @param destroy a function to destroy the data when the node is freed, or NULL
 */
void
md_node_set_data (MdNode         *node,
		  gpointer        data,
		  GDestroyNotify  destroy)
{
	/* md_node_lock (node); */

	node->data = data;
	node->data_destroy = destroy;

	/* md_node_unlock (node); */
}

/**
 * Retrieves the data attached to the node
 *
 * @param node the node
 * @returns the data, or NULL
 */
gpointer
md_node_get_data (MdNode *node)
{
	return node->data;
}

/**
 * Sets the GNode associated with this node.  Should only be used by MdTree
 *
 * @param node the node
 * @param gnode the GNode
 */
void
md_node_set_node (MdNode *node, GNode *gnode)
{
	node->node = gnode;
}

/**
 * Gets the GNode associated with this node.  Should only be used by MdTree
 *
 * @param node the node
 * @returns the GNode
 */
GNode *
md_node_get_node (MdNode *node)
{
	return node->node;
}

/**
 * Sets a flag on the node
 *
 * @param node the node
 * @param flag the flag
 */
void
md_node_set_flag (MdNode         *node,
		  int             flag)
{
	node->flags |= flag;
}

/**
 * Removes a flag on the node
 *
 * @param node the node
 * @param flag the flag
 */
void
md_node_unset_flag (MdNode         *node,
		    int             flag)
{
	node->flags &= ~flag;
}

/**
 * Checks whether a node has a given flag
 *
 * @param node the node
 * @param flag the flag
 * @returns TRUE if the node has the flag, FALSE otherwise
 */
gboolean
md_node_has_flag (MdNode         *node,
		  int             flag)
{
	return node->flags & flag;
}

/** @} */
