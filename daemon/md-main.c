/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <glib.h>
#include <popt.h>
#include "md-server.h"
#include "md-main.h"
#include "md-debug.h"

#define DIE_TIMEOUT_MILLIS 1000*60 /* one minute */

static guint die_timeout = -1;

static gboolean
md_die_timeout_cb (gpointer data)
{
	md_debug (DEBUG_INFO, "Today is a good day to die...");

	md_server_deinit ();
	exit (0);
	
	return FALSE;
}

void
md_start_die_timeout (void)
{
	die_timeout = g_timeout_add (DIE_TIMEOUT_MILLIS,
				     md_die_timeout_cb,
				     NULL);
}

void
md_cancel_die_timeout (void)
{
	if (die_timeout > 0) {
		g_source_remove (die_timeout);
		die_timeout = -1;
	}
}

static gboolean
md_init (void)
{
	struct sigaction sa;

	memset (&sa, 0, sizeof (sa));
	sa.sa_handler = SIG_IGN;
	sigaction (SIGPIPE, &sa, 0);

	if (!md_server_init ())
		return FALSE;

	return TRUE;
}



static void
md_main_loop (void)
{
	GMainLoop *loop;

	loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (loop);

}

int
main (int argc, const char *argv[])
{
	g_thread_init (NULL);

	if (!g_thread_supported ()) {
		g_error ("The glib thread library does not support your system."
			 "  Thank you for playing, goodbye.");
	}

	if (!md_init ()) {
		g_error ("Couldn't initialize marmot.  Exiting...");
		return -1;
	}

	md_main_loop ();

	return 0;
}
