
#ifndef __MD_POLL_H__
#define __MD_POLL_H__

#include <glib.h>
#include "md-subscription.h"

G_BEGIN_DECLS

typedef void (*MdPollHandler) (const char *path, gboolean added);

gboolean   md_poll_init_full             (gboolean start_scan_thread);

gboolean   md_poll_init                  (void);

gboolean   md_poll_add_subscription      (MdSubscription *sub);

gboolean   md_poll_remove_subscription   (MdSubscription *sub);

gboolean   md_poll_remove_all_for        (MdListener *listener);

void       md_poll_set_directory_handler (MdPollHandler handler);
void       md_poll_set_file_handler      (MdPollHandler handler);

void       md_poll_scan_directory        (const char *path,
					  GList      *exist_subs);

void       md_poll_consume_subscriptions (void);
						 
G_END_DECLS

#endif /* __MD_POLL_H__ */
