/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <config.h>
#define _GNU_SOURCE
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <glib.h>
#include "md-debug.h"
#include "md-poll.h"
#include "md-dnotify.h"
#include "md-tree.h"
#include "md-event.h"
#include "md-server.h"
#include "marmot-events.h"

/* just pulling a value out of nowhere here...may need tweaking */
#define MAX_QUEUE_SIZE 500

typedef struct {
	char *path;
	int fd;
	int refcount;
} DNotifyData;

static GHashTable *path_hash = NULL;
static GHashTable *fd_hash = NULL;
G_LOCK_DEFINE_STATIC (dnotify);

/* TODO: GQueue is not signal-safe, need to use something else */
static GQueue *changes = NULL;

static GMainContext *loop_context;
static GIOChannel *pipe_read_ioc = NULL;
static GIOChannel *pipe_write_ioc = NULL;

static gboolean have_consume_idler = FALSE;

static DNotifyData *
md_dnotify_data_new (const char *path, int fd)
{
	DNotifyData *data;

	data = g_new0 (DNotifyData, 1);
	data->path = g_strdup (path);
	data->fd = fd;
	data->refcount = 1;

	return data;
}

static void
md_dnotify_data_free (DNotifyData *data)
{
	g_free (data->path);
	g_free (data);
}

static void
md_dnotify_directory_handler (const char *path, gboolean added)
{
	DNotifyData *data;
	int fd;

	G_LOCK (dnotify);

	if (added) {

		if ((data = g_hash_table_lookup (path_hash, path)) != NULL) {
			data->refcount++;
			G_UNLOCK (dnotify);
			return;
		}

		fd = open (path, O_RDONLY);

		if (fd < 0) {
			G_UNLOCK (dnotify);
			return;
		}

		data = md_dnotify_data_new (path, fd);
		g_hash_table_insert (fd_hash, GINT_TO_POINTER (data->fd), data);
		g_hash_table_insert (path_hash, data->path, data);
		
		fcntl (fd, F_SETSIG, SIGRTMIN);
		fcntl (fd, F_NOTIFY, DN_MODIFY|DN_CREATE|DN_DELETE|DN_RENAME|DN_ATTRIB|DN_MULTISHOT);
	} else {
		data = g_hash_table_lookup (path_hash, path);

		if (!data) {
			G_UNLOCK (dnotify);
			return;
		}

		data->refcount--;

		if (data->refcount == 0) {
			close (data->fd);
			g_hash_table_remove (path_hash, data->path);
			g_hash_table_remove (fd_hash, GINT_TO_POINTER (data->fd));
			md_dnotify_data_free (data);
		}
	}

	G_UNLOCK (dnotify);
}

static void
md_dnotify_file_handler (const char *path, gboolean added)
{
	char *dir;

	dir = g_path_get_dirname (path);
	md_dnotify_directory_handler (dir, added);
	g_free (dir);
}

static void
dnotify_signal_handler (int sig, siginfo_t *si, void *sig_data)
{
	if (changes->length > MAX_QUEUE_SIZE) {
		md_debug (DEBUG_INFO, "Queue Full");
		return;
	}

	g_queue_push_head (changes, GINT_TO_POINTER (si->si_fd));

	g_io_channel_write_chars (pipe_write_ioc, "bogus",
				  5, NULL, NULL);
	g_io_channel_flush (pipe_write_ioc, NULL);
}

static void
overflow_signal_handler (int sig, siginfo_t *si, void *sig_data)
{
	md_debug (DEBUG_INFO, "**** signal queue overflow ***");
}

static gpointer
md_dnotify_scan_loop (gpointer data)
{
	g_main_loop_run (g_main_loop_new (loop_context, TRUE));
}

static gboolean
md_dnotify_pipe_handler (gpointer user_data)
{
	char buf[5000];
	DNotifyData *data;
	gpointer fd;
	int i;

	g_io_channel_read_chars (pipe_read_ioc, buf, sizeof (buf),
				 NULL, NULL);

	i = 0;
	while ((fd = g_queue_pop_tail (changes)) != NULL) {

		G_LOCK (dnotify);
		data = g_hash_table_lookup (fd_hash, fd);
		G_UNLOCK (dnotify);

		if (data == NULL)
			continue;

		md_poll_scan_directory (data->path, NULL);
		i++;
	}

	return TRUE;
}

static gboolean
md_dnotify_consume_subscriptions_real (gpointer data)
{
	md_poll_consume_subscriptions ();
	have_consume_idler = FALSE;
	return FALSE;
}

static void
md_dnotify_consume_subscriptions (void)
{
	GSource *source;

	if (have_consume_idler)
		return;
	
	have_consume_idler = TRUE;
	source = g_idle_source_new ();
	g_source_set_callback (source, md_dnotify_consume_subscriptions_real,
			       NULL, NULL);
	g_source_attach (source, loop_context);
}

/**
 * @defgroup DNotify DNotify Backend
 * @ingroup Backends
 * @brief DNotify backend API
 *
 * Since version 2.4, Linux kernels have included the Linux Directory
 * Notification system (dnotify).  This backend uses dnotify to know when
 * files are changed/created/deleted.  Since dnotify doesn't tell us
 * exactly what event happened to which file (just that some even happened
 * in some directory), we still have to cache stat() information.  For this,
 * we can just use the code in the polling backend.
 *
 * @{
 */


/**
 * Initializes the polling system.  This must be called before
 * any other functions in this module.
 *
 * @returns TRUE if initialization succeeded, FALSE otherwise
 */
gboolean
md_dnotify_init (void)
{
	struct sigaction act;
	int fds[2];
	GSource *source;

	g_return_val_if_fail (md_poll_init_full (FALSE), FALSE);

	if (pipe (fds) < 0) {
		g_warning ("Could not create pipe.");
		return FALSE;
	}

	pipe_read_ioc = g_io_channel_unix_new (fds[0]);
	pipe_write_ioc = g_io_channel_unix_new (fds[1]);

	g_io_channel_set_flags (pipe_read_ioc, G_IO_FLAG_NONBLOCK, NULL);
	g_io_channel_set_flags (pipe_write_ioc, G_IO_FLAG_NONBLOCK, NULL);

	loop_context = g_main_context_new ();
	
	source = g_io_create_watch (pipe_read_ioc,
				    G_IO_IN | G_IO_HUP | G_IO_ERR);
	g_source_set_callback (source, md_dnotify_pipe_handler, NULL, NULL);
	g_source_attach (source, loop_context);

	/* setup some signal stuff */
	act.sa_sigaction = dnotify_signal_handler;
	sigemptyset (&act.sa_mask);
	act.sa_flags = SA_SIGINFO;
	sigaction (SIGRTMIN, &act, NULL);

	/* catch SIGIO as well (happens when the realtime queue fills up) */
	act.sa_sigaction = overflow_signal_handler;
	sigemptyset (&act.sa_mask);
	sigaction (SIGIO, &act, NULL);

	changes = g_queue_new ();

	g_thread_create (md_dnotify_scan_loop, NULL, TRUE, NULL);

	path_hash = g_hash_table_new (g_str_hash, g_str_equal);
	fd_hash = g_hash_table_new (g_direct_hash, g_direct_equal);
	md_poll_set_directory_handler (md_dnotify_directory_handler);
	md_poll_set_file_handler (md_dnotify_file_handler);
	
	return TRUE;
}

/**
 * Adds a subscription to be monitored.
 *
 * @param sub a #MdSubscription to be polled
 * @returns TRUE if adding the subscription succeeded, FALSE otherwise
 */
gboolean
md_dnotify_add_subscription (MdSubscription *sub)
{
	if (!md_poll_add_subscription (sub)) {
		return FALSE;
	}

	if (md_subscription_is_dir (sub)) {
		md_dnotify_consume_subscriptions ();
	}

	return TRUE;
}

/**
 * Removes a subscription which was being monitored.
 *
 * @param sub a #MdSubscription to remove
 * @returns TRUE if removing the subscription succeeded, FALSE otherwise
 */
gboolean
md_dnotify_remove_subscription (MdSubscription *sub)
{
	if (!md_poll_remove_subscription (sub)) {
		return FALSE;
	}

	if (md_subscription_is_dir (sub)) {
		md_dnotify_consume_subscriptions ();
	}

	return TRUE;
}

/**
 * Stop monitoring all subscriptions for a given listener.
 *
 * @param listener a #MdListener
 * @returns TRUE if removing the subscriptions succeeded, FALSE otherwise
 */
gboolean
md_dnotify_remove_all_for (MdListener *listener)
{
	if (!md_poll_remove_all_for (listener)) {
		return FALSE;
	}

	md_dnotify_consume_subscriptions ();

	return TRUE;
}

/** @} */
