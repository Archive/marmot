
#ifndef __MD_DNOTIFY_H__
#define __MD_DNOTIFY_H__

#include <glib.h>
#include "md-poll.h"
#include "md-subscription.h"

G_BEGIN_DECLS

gboolean   md_dnotify_init                  (void);
gboolean   md_dnotify_add_subscription      (MdSubscription *sub);
gboolean   md_dnotify_remove_subscription   (MdSubscription *sub);
gboolean   md_dnotify_remove_all_for        (MdListener *listener);

G_END_DECLS

#endif /* __MD_DNOTIFY_H__ */
