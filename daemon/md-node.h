
#ifndef __MD_NODE_H__
#define __MD_NODE_H__

#include <glib.h>
#include "marmot-events.h"
#include "md-subscription.h"

G_BEGIN_DECLS

typedef struct _MdNode MdNode;

typedef gboolean (*MdSubFilterFunc) (MdSubscription *sub);

struct _MdNode {
	char *path;

	GMutex *lock;
	GList *subs;
	gboolean is_dir;
	gpointer data;
	GDestroyNotify data_destroy;

	int flags;

	GNode *node;
};

#define md_node_lock(x) g_mutex_lock (x->lock)
#define md_node_unlock(x) g_mutex_unlock (x->lock)

MdNode               *md_node_new                 (const char     *path,
						   MdSubscription *initial_sub,
						   gboolean        is_dir);

void                  md_node_free                (MdNode         *node);

MdNode               *md_node_parent              (MdNode         *node);

gboolean              md_node_is_dir              (MdNode         *node);

void                  md_node_set_is_dir          (MdNode         *node,
						   gboolean        is_dir);
	
G_CONST_RETURN char  *md_node_get_path            (MdNode         *node);

GList                *md_node_get_subscriptions   (MdNode         *node);

gboolean              md_node_add_subscription    (MdNode         *node,
						   MdSubscription *sub);

gboolean              md_node_remove_subscription (MdNode         *node,
						   MdSubscription *sub);

int                   md_node_copy_subscriptions  (MdNode         *src,
						   MdNode         *dest,
						   MdSubFilterFunc filter);

gboolean              md_node_has_recursive_sub   (MdNode         *node);

void                  md_node_set_node            (MdNode         *node,
						   GNode          *gnode);
GNode                *md_node_get_node            (MdNode         *node);

void                  md_node_set_data            (MdNode         *node,
						   gpointer        data,
						   GDestroyNotify  destroy);

gpointer              md_node_get_data            (MdNode         *node);

void                  md_node_set_flag            (MdNode         *node,
						   int             flag);
void                  md_node_unset_flag          (MdNode         *node,
						   int             flag);
gboolean              md_node_has_flag            (MdNode         *node,
						   int             flag);

G_END_DECLS

#endif /* __MD_NODE_H__ */
