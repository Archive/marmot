/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <glib.h>
#include <dbus/dbus.h>
#include "md-debug.h"
#include "md-tree.h"
#include "md-poll.h"
#include "md-event.h"
#include "md-server.h"
#include "marmot-events.h"

#define DEFAULT_POLL_TIMEOUT 3

#define FLAG_NEW_NODE 1 << 5

typedef struct {
	gboolean exists;
	char *path;
	struct stat sbuf;
} MdPollData;

static MdTree *tree = NULL;

static GList *new_subs = NULL;
G_LOCK_DEFINE_STATIC (new_subs);

static GList *removed_subs = NULL;
G_LOCK_DEFINE_STATIC (removed_subs);

static MdPollHandler dir_handler = NULL;
static MdPollHandler file_handler = NULL;

static void
trigger_dir_handler (const char *path, gboolean added)
{
	if (dir_handler != NULL)
		(* dir_handler) (path, added);
}

static void
trigger_file_handler (const char *path, gboolean added)
{
	if (file_handler != NULL)
		(* file_handler) (path, added);
}

static void
node_add_subscription (MdNode *node, MdSubscription *sub)
{
	md_node_add_subscription (node, sub);

	if (md_node_is_dir (node))
		trigger_dir_handler (md_node_get_path (node), TRUE);
	else
		trigger_file_handler (md_node_get_path (node), TRUE);

}

static void
node_remove_subscription (MdNode *node, MdSubscription *sub)
{
	md_node_remove_subscription (node, sub);

	if (md_node_is_dir (node))
		trigger_dir_handler (md_node_get_path (node), FALSE);
	else
		trigger_file_handler (md_node_get_path (node), FALSE);
}

static MdPollData *
md_poll_data_new (const char *path)
{
	MdPollData *data;

	data = g_new0 (MdPollData, 1);
	data->path = g_strdup (path);

	data->exists = TRUE;

	return data;
}

static void
md_poll_emit_event (MdNode *node, MarmotEventType event, GList *exist_subs)
{
	GList *l;
	MdNode *parent;
	MdPollData *data;
	GList *subs;

	/* we only emit CREATED, DELETED and EXISTS for directories */
	if (md_node_is_dir (node) &&
	    !(event == MARMOT_EVENT_CREATED ||
	      event == MARMOT_EVENT_DELETED ||
	      event == MARMOT_EVENT_EXISTS))	
		return;

	subs = md_node_get_subscriptions (node);
	if (subs)
		subs = g_list_copy (subs);

	parent = md_node_parent (node);
	if (parent) {
		GList *parent_subs = md_node_get_subscriptions (parent);

		for (l = parent_subs; l; l = l->next) {
			if (!g_list_find (subs, l->data))
				subs = g_list_prepend (subs, l->data);
		}
	}

	if (exist_subs) {

		data = md_node_get_data (node);

		for (l = subs; l; l = l->next) {
			MdSubscription *sub = l->data;
			MarmotEventType new_event = event;

			if (g_list_find (exist_subs, sub)) {
			    if (data && data->exists)
				    new_event = MARMOT_EVENT_EXISTS;
			    else
				    continue;
			}

			md_server_emit_event (md_node_get_path (node),
					      new_event,
					      g_list_append (NULL, sub));
		}
	} else {
	       	md_server_emit_event (md_node_get_path (node), event, subs);
	}

	g_list_free (subs);
}

static void
md_poll_data_destroy (MdPollData *data)
{
	g_free (data->path);
	g_free (data);
}

static MarmotEventType
poll_file (MdNode *node)
{
	MdPollData *data;
	MarmotEventType event;
	struct stat sbuf;
	int stat_ret;

	data = md_node_get_data (node);
	if (data == NULL) {
		char real[PATH_MAX];

		realpath (md_node_get_path (node), real);
		data = md_poll_data_new (real);

		md_node_set_data (node, data,
				  (GDestroyNotify)md_poll_data_destroy);

		stat_ret = stat (data->path, &sbuf);
		data->exists = (stat_ret == 0);
		data->sbuf = sbuf;

		if (data->exists)
			return 0;
		else
			return MARMOT_EVENT_DELETED;
	}
	
	event = 0;

	if (stat (data->path, &sbuf) != 0) {
		if (errno == ENOENT && data->exists) {
			/* deleted */
			data->exists = FALSE;
			event = MARMOT_EVENT_DELETED;
		}
	} else if (!data->exists) {
		/* created */
		data->exists = TRUE;
		event = MARMOT_EVENT_CREATED;
	} else if ((data->sbuf.st_mtime != sbuf.st_mtime) ||
		   (data->sbuf.st_ctime != sbuf.st_ctime)) {
		event = MARMOT_EVENT_CHANGED;
	}
	
	data->sbuf = sbuf;

	return event;
}

static GList *
list_intersection (GList *a, GList *b)
{
	GList *ret, *l;

	ret = NULL;
	for (l = a; l; l = l->next) {
		if (g_list_find (b, l->data))
			ret = g_list_prepend (ret, l->data);
	}

	return ret;
}

static void
md_poll_scan_directory_internal (MdNode *dir_node, GList *exist_subs,
				 gboolean scan_for_new)
{
	GDir *dir;
	const char *name;
	char *path;
	MdNode *node;
	MarmotEventType event = 0;
	GList *dir_exist_subs = NULL;
	GList *children, *l;
	gboolean recursive;
	unsigned int i;

	g_return_if_fail (dir_node != NULL);

	if (!scan_for_new)
		goto scan_files;

	if (!md_node_get_subscriptions (dir_node))
		goto scan_files;

	event = poll_file (dir_node);
	dir_exist_subs = list_intersection (exist_subs, md_node_get_subscriptions (dir_node));
	recursive = md_node_has_recursive_sub (dir_node);

	if (event == 0 && !dir_exist_subs)
		goto scan_files;

	md_poll_emit_event (dir_node, event, dir_exist_subs);
	
	dir = g_dir_open (md_node_get_path (dir_node), 0, NULL);
	
	if (dir == NULL)
		goto scan_files;

	while ((name = g_dir_read_name (dir)) != NULL) {
		path = g_build_filename (md_node_get_path (dir_node),
					 name, NULL);

		node = md_tree_get_at_path (tree, path);
		
		if (!node) {
			if (!g_file_test (path, G_FILE_TEST_IS_DIR)) {
				node = md_node_new (path, NULL, FALSE);
				md_tree_add (tree, dir_node, node);
				md_node_set_flag (node, FLAG_NEW_NODE);
			} else {
				node = md_node_new (path, NULL, TRUE);
				md_tree_add (tree, dir_node, node);

			       	if (recursive) {
					md_node_copy_subscriptions (dir_node,
								    node,
								    md_subscription_is_recursive);

					/* FIXME: this is totally crappy */
					for (i = 0; i < g_list_length (md_node_get_subscriptions (node)); i++) {
						trigger_dir_handler (md_node_get_path (node), TRUE);
					}
				}

				md_node_set_flag (node, FLAG_NEW_NODE);
			} 
		}

		g_free (path);
	}

	g_dir_close (dir);

scan_files:

	children = md_tree_get_children (tree, dir_node);
	for (l = children; l; l = l->next) {
		node = (MdNode *)l->data;

		event = poll_file (node);
		
		if (md_node_is_dir (node) &&
		    md_node_has_flag (node, FLAG_NEW_NODE) &&
		    md_node_get_subscriptions (node)) {
			md_node_unset_flag (node, FLAG_NEW_NODE);
			md_poll_scan_directory_internal (node, exist_subs,
							 scan_for_new);
		} else if (md_node_has_flag (node, FLAG_NEW_NODE)) {
			md_node_unset_flag (node, FLAG_NEW_NODE);
			event = MARMOT_EVENT_CREATED;
		}

		if (event != 0) {
			md_poll_emit_event (node, event, exist_subs);
		} else {
			/* just send the EXIST events */

			md_server_emit_event (md_node_get_path (node),
					      MARMOT_EVENT_EXISTS,
					      exist_subs);
			
		}
	}

	g_list_free (children);
	g_list_free (dir_exist_subs);
}

/*
static gboolean
md_poll_needs_scanned (MdNode *node)
{
	MarmotEventType event;
	
	g_return_val_if_fail (md_node_is_dir (node), FALSE);

	if (!md_node_get_subscriptions (node))
		return FALSE;

	event = poll_file (node);

	if (event != MARMOT_EVENT_DELETED &&
	    event != 0) {
		return TRUE;
	} else
		return FALSE;
}
*/

static gboolean
remove_directory_subscription (MdNode *node, MdSubscription *sub)
{
	GList *children, *l;
	gboolean recursive;
	gboolean remove_dir;

	node_remove_subscription (node, sub);

	remove_dir = md_node_get_subscriptions (node) == NULL;
	recursive = md_subscription_is_recursive (sub);

	children = md_tree_get_children (tree, node);
	for (l = children; l; l = l->next) {
		MdNode *child = (MdNode *)l->data;

		if (md_node_is_dir (child)) {
			if (remove_directory_subscription (child, sub) &&
			    remove_dir) {
				md_tree_remove (tree, child);
			} else {
				remove_dir = FALSE;
			}
		} else {
			node_remove_subscription (child, sub);

			if (!md_node_get_subscriptions (child) &&
			    remove_dir) {
				md_tree_remove (tree, child);
			} else {
				remove_dir = FALSE;
			}
		}
	}

	g_list_free (children);

	return remove_dir;
}

static gpointer
md_poll_scan_loop (gpointer data)
{
	GList *dirs, *l;

	for (;;) {
		g_usleep (DEFAULT_POLL_TIMEOUT*G_USEC_PER_SEC);

		md_poll_consume_subscriptions ();
		
		dirs = md_tree_get_directories (tree, NULL);
		for (l = dirs; l; l = l->next) {
			MdNode *node = (MdNode *)l->data;

			md_poll_scan_directory_internal (node, NULL, TRUE);
		}

		g_list_free (dirs);
	}
}

static void
prune_tree (MdNode *node)
{
	/* don't prune the root */
	if (md_node_parent (node) == NULL)
		return;

	/*
	g_message ("Prune: %s has children? %s - subs? %s",
		   md_node_get_path (node),
		   md_tree_has_children (tree, node) ? "TRUE" : "FALSE",
		   md_node_get_subscriptions (node) ? "TRUE" : "FALSE");
	*/

	if (!md_tree_has_children (tree, node) &&
	    !md_node_get_subscriptions (node)) {
		MdNode *parent;

		parent = md_node_parent (node);
		md_tree_remove (tree, node);
		prune_tree (parent);
	}
}





/**
 * @defgroup Polling Polling Backend
 * @ingroup Backends
 * @brief Polling backend API
 *
 * This is the default backend used in Marmot.  It basically just calls
 * stat() on files/directories every so often to see when things change.  The
 * statting happens in a separate thread, controllable with arguments to
 * #md_poll_init_full().
 *
 * @{
 */




/**
 * Initializes the polling system.  This must be called before
 * any other functions in this module.
 *
 * @param start_scan_thread TRUE if a separate scanning thread should be
 * started, FALSE otherwise.
 * @returns TRUE if initialization succeeded, FALSE otherwise
 */
gboolean
md_poll_init_full (gboolean start_scan_thread)
{
	tree = md_tree_new ();

	if (start_scan_thread)
		g_thread_create (md_poll_scan_loop, NULL, TRUE, NULL);

	return TRUE;
}

/**
 * Initializes the polling system.  This must be called before
 * any other functions in this module.
 *
 * This function simply calls #md_poll_init_full (TRUE)
 *
 * @returns TRUE if initialization succeeded, FALSE otherwise
 */
gboolean
md_poll_init (void)
{
	return md_poll_init_full (TRUE);	
}

/**
 * Adds a subscription to be polled.
 *
 * @param sub a #MdSubscription to be polled
 * @returns TRUE if adding the subscription succeeded, FALSE otherwise
 */
gboolean
md_poll_add_subscription (MdSubscription *sub)
{
	/*
	node = md_tree_get_at_path (tree, md_subscription_get_path (sub));
	if (!node) {
		node = md_tree_add_at_path (tree,
					    md_subscription_get_path (sub),
					    md_subscription_is_dir (sub));

		if (dir_handler &&
		    md_subscription_is_dir (sub)) {
			
			(*dir_handler) (md_node_get_path (node), TRUE);

		} else if (file_handler &&
			 !md_subscription_is_dir (sub)) {

			(*file_handler) (md_node_get_path (node), TRUE);
		}

	} else if (md_subscription_is_dir (sub) &&
		   md_subscription_is_recursive (sub)) {
		GList *dirs, *l;

		dirs = md_tree_get_directories (tree, node);

		for (l = dirs; l; l = l->next) {
			MdNode *child = l->data;

			md_node_add_subscription (child, sub);
		}

		g_list_free (dirs);
	}

	md_node_add_subscription (node, sub);
	*/


	/*
	if (md_subscription_is_dir (sub)) {
		G_LOCK (new_subs);
		new_subs = g_list_prepend (new_subs, sub);
		G_UNLOCK (new_subs);
	}
	*/

	md_listener_add_subscription (md_subscription_get_listener (sub), sub);

	G_LOCK (new_subs);
	new_subs = g_list_prepend (new_subs, sub);
	G_UNLOCK (new_subs);

	return TRUE;
}

/**
 * Removes a subscription which was being polled.
 *
 * @param sub a #MdSubscription to remove
 * @returns TRUE if removing the subscription succeeded, FALSE otherwise
 */
gboolean
md_poll_remove_subscription (MdSubscription *sub)
{
	MdNode *node;

	node = md_tree_get_at_path (tree, md_subscription_get_path (sub));
	if (node == NULL)
		return FALSE;

	md_subscription_cancel (sub);
	md_listener_remove_subscription (md_subscription_get_listener (sub), sub);

	G_LOCK (removed_subs);
	removed_subs = g_list_prepend (removed_subs, sub);
	G_UNLOCK (removed_subs);

	return TRUE;
}

/*
MdSubscription *
md_poll_get_subscription (MdListener *listener,
			  const char *path)
{
	MdNode *node;
	GList *l;

	node = md_tree_get_at_path (tree, path);
	if (!node)
		return NULL;

	for (l = md_node_get_subscriptions (node); l; l = l->next) {
		MdSubscription *sub = l->data;

		if (md_subscription_get_listener (sub) == listener)
			return sub;
	}

	return NULL;
}
*/

/**
 * Stop polling all subscriptions for a given #MdListener.
 *
 * @param listener a #MdListener
 * @returns TRUE if removing the subscriptions succeeded, FALSE otherwise
 */
gboolean
md_poll_remove_all_for (MdListener *listener)
{
	GList *subs, *l = NULL;

	subs = md_listener_get_subscriptions (listener);

	for (l = subs; l; l = l->next) {
		MdSubscription *sub = l->data;

		g_assert (sub != NULL);

		md_poll_remove_subscription (sub);
	}

	if (subs) {
		g_list_free (subs);
		return TRUE;
	} else
		return FALSE;
}

/**
 * Scans a directory for changes, and emits events if needed.
 *
 * @param path the path to the directory to be scanned
 * @param exist_subs a list of type #MdSubscription of new subscriptions
 * which need to be sent the EXIST event.
 */
void
md_poll_scan_directory (const char *path, GList *exist_subs)
{
	MdNode *node;

	node = md_tree_get_at_path (tree, path);
	if (node == NULL)
		node = md_tree_add_at_path (tree, path, TRUE);

	md_poll_scan_directory_internal (node, exist_subs, TRUE);
}

/**
 * Commits all pending added/removed subscriptions.  For new subscriptions,
 * this includes scanning directories.
 *
 */
void
md_poll_consume_subscriptions (void)
{
	GList *subs, *l;

	/* check for new dir subs which need special handling
	 * (specifically, sending them the EXIST event)
	 */
	G_LOCK (new_subs);
	if (new_subs != NULL) {
		/* we don't want to block the main loop */
		subs = new_subs;
		new_subs = NULL;
		G_UNLOCK (new_subs);
		
		md_debug (DEBUG_INFO,
			  "%d new subscriptions.",
			  g_list_length (subs));

		for (l = subs; l; l = l->next) {
			MdSubscription *sub = l->data;
			MdNode *node = md_tree_get_at_path (tree,
					md_subscription_get_path (sub));

			if (!node) {
				node = md_tree_add_at_path (tree,
						md_subscription_get_path (sub),
						md_subscription_is_dir (sub));
			}

			node_add_subscription (node, sub);

			if (md_node_is_dir (node)) {
				md_debug (DEBUG_INFO,
					  "Looking for existing files in: %s...",
					  md_node_get_path (node));
				
				md_poll_scan_directory_internal (node, subs,
								 TRUE);

				md_debug (DEBUG_INFO, "Done scanning %s",
					  md_node_get_path (node));
			}
		}

		g_list_free (subs);
	} else {
		G_UNLOCK (new_subs);
	}

	/* check for things that have been removed, and remove them */
	G_LOCK (removed_subs);
	if (removed_subs != NULL) {
		subs = removed_subs;
		removed_subs = NULL;
		G_UNLOCK (removed_subs);

		md_debug (DEBUG_INFO, "Tree has %d nodes",
			  md_tree_get_size (tree));
		for (l = subs; l; l = l->next) {
			MdSubscription *sub = l->data;
			MdNode *node = md_tree_get_at_path (tree,
					md_subscription_get_path (sub));

			md_debug (DEBUG_INFO, "Removing: %s",
				  md_subscription_get_path (sub));
			if (!md_node_is_dir (node)) {
				node_remove_subscription (node, sub);

				if (!md_node_get_subscriptions (node)) {
					MdNode *parent;

					parent = md_node_parent (node);
					md_tree_remove (tree, node);

					prune_tree (parent);
				}
			} else {
				if (remove_directory_subscription (node, sub)) {
					MdNode *parent;

					parent = md_node_parent (node);
					md_tree_remove (tree, node);

					prune_tree (parent);
				}
			}
			

		}
		g_list_free (subs);

		md_debug (DEBUG_INFO, "Tree has %d nodes",
			  md_tree_get_size (tree));

	} else {
		G_UNLOCK (removed_subs);
	}
}


/**
 * Sets the function to be called when a directory node loses or gains
 * subscriptions.  This is useful for implementing other sorts of backends
 * that want to use portions of this backend.
 *
 * @param handler a #MdPollHandler
 */
void
md_poll_set_directory_handler (MdPollHandler handler)
{
	dir_handler = handler;
}

/**
 * Sets the function to be called when a file node loses or gains
 * subscriptions.  This is useful for implementing other sorts of backends
 * that want to use portions of this backend.
 *
 * @param handler a #MdPollHandler
 */
void
md_poll_set_file_handler (MdPollHandler handler)
{
	file_handler = handler;
}

/** @} */
