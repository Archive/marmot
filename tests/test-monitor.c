/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include "marmot.h"

static gboolean
event_cb (Marmot *monitor,
	  const char *monitor_path,
	  const char *event_path,
	  MarmotEventType event,
	  gpointer user_data)
{
	switch (event) {
	case MARMOT_EVENT_EXISTS:
		g_print ("(%s) %s Exists\n", monitor_path, event_path);
		break;
	case MARMOT_EVENT_DELETED:
		g_print ("(%s) %s was Deleted\n", monitor_path, event_path);
		break;
	case MARMOT_EVENT_CREATED:
		g_print ("(%s) %s was Created\n", monitor_path, event_path);
		break;
	case MARMOT_EVENT_CHANGED:
		g_print ("(%s) %s was Changed\n", monitor_path, event_path);
		break;
	default:
		g_print ("(%s) %s had an unknown event\n",
			 monitor_path, event_path);
		break;
	}

	return TRUE;
}

int
main (int argc, char *argv[])
{
	gboolean success = FALSE;
	char buf[2];
	char *path = NULL;

	Marmot *monitor;

	if (argc < 3) {
		g_print ("Usage: test-monitor [--file, --dir] [--recursive, --filter <filter>] <path>\n");
		exit (0);
	}

	if (!marmot_init ()) {
		g_error ("Marmot init failed, bailing out...");
		exit (0);
	}

	monitor = marmot_monitor_new();
	if (monitor == NULL) {
		g_error ("Marmot Was Null...");
		exit (0);
	}

	if (strcmp (argv[1], "--file") == 0) {

		path = argv[2];
		success = marmot_subscribe_file (monitor, path,
						   MARMOT_EVENT_DELETED|
						   MARMOT_EVENT_CHANGED|
						   MARMOT_EVENT_CREATED,
						   event_cb, NULL);
	} else if (strcmp (argv[1], "--dir") == 0) {
		gboolean recursive = FALSE;
		path = argv[2];
		char *filter = NULL;

		if (argc >= 4 && strcmp (argv[2], "--recursive") == 0) {
			path = argv[3];
			recursive = TRUE;
		}

		if (argc >= 6 && strcmp (argv[3], "--filter") == 0) {
			filter = argv[4];
			path = argv[5];
		}

		g_message ("Passing filter: %s", filter);
		success = marmot_subscribe_directory (monitor, path,
							MARMOT_EVENT_DELETED|
							MARMOT_EVENT_CHANGED|
							MARMOT_EVENT_CREATED,
							recursive, filter,
							event_cb,
							NULL);
	}

	g_return_val_if_fail (success, 1);
		
	g_print ("Press enter to cancel monitoring\n");
	fgets (buf, 2, stdin);

	marmot_cancel (monitor, path);
	marmot_monitor_destroy (monitor);

	return 0;
}
