/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <stdio.h>
#include <marmot/marmot.h>

static gboolean
my_callback (Marmot   *monitor,
	     const char        *monitor_path,
	     const char        *event_path,
	     MarmotEventType  event,
	     gpointer           user_data)
{
	switch (event) {
	case MARMOT_EVENT_EXISTS:
		printf ("(%s) %s Exists\n", monitor_path, event_path);
		break;
	case MARMOT_EVENT_DELETED:
		printf ("(%s) %s was Deleted\n", monitor_path, event_path);
		break;
	case MARMOT_EVENT_CREATED:
		printf ("(%s) %s was Created\n", monitor_path, event_path);
		break;
	case MARMOT_EVENT_CHANGED:
		printf ("(%s) %s was Changed\n", monitor_path, event_path);
		break;
	default:
		printf ("(%s) %s had an unknown event\n",
			 monitor_path, event_path);
		break;
	}

	/* we still want to receive events */
	return TRUE;
}

int
main (int argc, char *argv[])
{
 	Marmot *monitor;

	/* init */
	marmot_init ();

	/* create a monitor */
	monitor = marmot_monitor_new ();

	/* subscribe to a directory */
	marmot_subscribe_directory (monitor, "/tmp",
				      MARMOT_EVENT_CREATED|
				      MARMOT_EVENT_DELETED,
				      FALSE, NULL,
				      my_callback,
				      NULL);

	/* go do some stuff, or just block in some way */

	/* cancel the subscription and destroy the monitor */
	marmot_cancel (monitor, "/tmp");
	marmot_monitor_destroy (monitor);

	return 0;
}
