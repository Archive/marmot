
#ifndef __MARMOT_MONITOR_H__
#define __MARMOT_MONITOR_H__

#include <glib.h>
#include "marmot-events.h"

G_BEGIN_DECLS

typedef struct _Marmot Marmot;

typedef gboolean (* MarmotEventCallbackFn)        (Marmot *monitor,
						     const char *monitor_path,
						     const char *event_path,
						     MarmotEventType event,
						     gpointer user_data);

gboolean         marmot_init                      (void);

Marmot	*marmot_monitor_new               (void);

void             marmot_monitor_destroy           (Marmot *monitor);

gboolean         marmot_subscribe_file            (Marmot *monitor,
						     const char *path,
						     int events,
						     MarmotEventCallbackFn cb,
						     gpointer user_data);

gboolean         marmot_subscribe_directory       (Marmot *monitor,
						     const char *path,
						     int events,
						     gboolean recursive,
						     const char *filter,
						     MarmotEventCallbackFn cb,
						     gpointer user_data);

void		 marmot_cancel                    (Marmot *monitor,
						     const char *path);

G_END_DECLS

#endif /* __MARMOT_MONITOR_H__ */
