/* Marmot
 * Copyright (C) 2003 James Willcox, Corey Bowers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <glib.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include "marmot.h"
#include "md-dbus.h"
#include "md-debug.h"

struct _Marmot {
	GMutex *lock;
	GHashTable *listeners;
	char *id;
};

typedef struct _MarmotListener {
	char *path;
	MarmotEventCallbackFn callback;
	gpointer user_data;
} MarmotListener; 

static DBusConnection *dbus_conn;
G_LOCK_DEFINE_STATIC(dbus_conn);

static gboolean have_init = FALSE;
static int num_listeners = 0;
static GHashTable *monitor_hash;
G_LOCK_DEFINE_STATIC(monitor_hash);

static DBusHandlerResult
marmot_monitor_message_handler (DBusMessageHandler *handler,
				  DBusConnection     *connection,
				  DBusMessage        *message,
				  void               *user_data)
{
	DBusError error;
	Marmot *monitor;
	MarmotListener *listener;
	char* id;
	char* monitor_path;
	char* event_path;
	MarmotEventType event;

	dbus_error_init (&error);
	dbus_message_get_args(message, &error,
			      DBUS_TYPE_STRING, &id,
			      DBUS_TYPE_STRING, &monitor_path,
			      DBUS_TYPE_STRING, &event_path,
			      DBUS_TYPE_UINT32, &event, 0);

	if (dbus_error_is_set (&error)) {
		md_debug (DEBUG_INFO, "Error receiving message: %s", error.message);
		dbus_error_free (&error);
		return DBUS_HANDLER_RESULT_ALLOW_MORE_HANDLERS;
	}

	G_LOCK (monitor_hash);
	monitor = g_hash_table_lookup (monitor_hash, id);
	G_UNLOCK (monitor_hash);

	g_mutex_lock (monitor->lock);
	listener = g_hash_table_lookup (monitor->listeners, monitor_path);
	g_mutex_unlock (monitor->lock);
	
	if (listener->callback (monitor, monitor_path, event_path, event,
				listener->user_data) == FALSE) {
		marmot_cancel (monitor, monitor_path);
	}

	return DBUS_HANDLER_RESULT_ALLOW_MORE_HANDLERS;
}


static gpointer
marmot_monitor_main_loop (gpointer data)
{
	DBusMessageHandler *handler;
	const char *messages[] = { MARMOT_MESSAGE_EVENT };
	GMainContext *context = g_main_context_new ();
	
	dbus_connection_setup_with_g_main (dbus_conn, context);

	handler = dbus_message_handler_new (marmot_monitor_message_handler,
					    NULL, NULL);

	if (!dbus_connection_register_handler (dbus_conn, handler, messages,
					       G_N_ELEMENTS (messages))) {
		md_debug (DEBUG_INFO, "Could not register message handler!");
	}

	g_main_loop_run (g_main_loop_new (context, TRUE));

	return NULL;
}

static MarmotListener *
marmot_listener_new (const char *path,
		      MarmotEventCallbackFn cb,
		      gpointer user_data)
{
	MarmotListener *listener;

	listener = g_new (MarmotListener, 1);
	listener->path = g_strdup (path);
	listener->callback = cb;
	listener->user_data = user_data;

	return listener;
}

static void
marmot_listener_free (MarmotListener *listener)
{
	g_return_if_fail (listener != NULL);

	g_free (listener->path);
	g_free (listener);
}

static void
marmot_monitor_send_cancel (Marmot *monitor,
			      const char *path)
{
	DBusMessage *msg;

	msg = dbus_message_new (MARMOT_MESSAGE_CANCEL,
				MARMOT_SERVICE_NAME);
	dbus_message_append_args (msg, DBUS_TYPE_STRING, monitor->id,
				  DBUS_TYPE_STRING, path, 0);

	G_LOCK (dbus_conn);
	if (!dbus_connection_send (dbus_conn, msg, NULL)) {
		md_debug (DEBUG_INFO, "Couldn't cancel %s", path);
	} else {
		md_debug (DEBUG_INFO, "Sent message: %s", MARMOT_MESSAGE_CANCEL);
	}

	G_UNLOCK (dbus_conn);
}

static void
marmot_monitor_cancel_foreach (gpointer key, gpointer value, gpointer user_data)
{
	marmot_monitor_send_cancel ((Marmot *)user_data, (char *)key);
}

/*
static gboolean
activate_marmot (void)
{
	DBusMessage *msg;
	DBusMessage *reply;
	DBusError error;

	msg = dbus_message_new (DBUS_MESSAGE_ACTIVATE_SERVICE,
				DBUS_SERVICE_DBUS);

	dbus_message_append_args (msg, DBUS_TYPE_STRING, MARMOT_SERVICE_NAME,
				  DBUS_TYPE_UINT32, 0, 0);

	dbus_error_init (&error);
	reply = dbus_connection_send_with_reply_and_block (dbus_conn, msg,
							   500, &error);
	if (reply && !dbus_message_get_is_error (reply)) {
		dbus_message_unref (reply);
		return TRUE;
	} else {
		md_debug (DEBUG_INFO, "Activation error: %s", error.message);
	}

	return FALSE;
}
*/

/**
 * @defgroup Lib Client Library
 * @brief Client library for applications to use Marmot.
 * @{
 *
 * The Marmot client library provides a simple API for receiving events
 * when files or directories are altered.  Any of #MarmotEventType can occur
 * for a file, but only create, delete, and exist are emitted for directories
 * themselves.
 *
 * When a directory is initially monitored, an EXISTS event will be emitted
 * for every file in that directory (and directories beneath it if it was
 * a recursive subscription).  Applications should rely on this event to
 * determine the state of the directory and the files in it, rather than
 * assuming that they exist when the subscription is created.  This avoids
 * a race condition where file/directories are removed between the time
 * the subscription is created, and when they are actually monitored.
 *
 */

/**
 * @example example.c
 * A small example program
 *
 * compile with "gcc `pkg-config --cflags marmot` example.c -o example
 * `pkg-config --libs marmot`"
 *
 * Of course, you should check return values for errors in an actual
 * application.
 *
 */

/**
 * @typedef MarmotEventType
 *
 * An enumeration which represents possible events
 *
 * @param MARMOT_EVENT_CHANGED - the file changed in some way (write, truncate, permissions)
 * @param MARMOT_EVENT_CREATED - the file was created
 * @param MARMOT_EVENT_DELETED - the file was deleted
 * @param MARMOT_EVENT_MOVED - the file was moved
 * @param MARMOT_EVENT_EXISTS - the file exists
 *
 */

/**
 * @typedef MarmotEventCallbackFn
 *
 * The type of a function which will be called when events occur.
 *
 * @b WARNING!!  This will always be invoked from a separate thread within
 * the library, so anything an application does in an event callback
 * function needs to be thread-safe.
 *
 * @param monitor The Marmot the subscription was created with
 * @param monitor_path The path is being monitored (e.g., "/tmp")
 * @param event_path The path the event occured on (e.g., "/tmp/foobar")
 * @param event The event that occured (e.g., MARMOT_EVENT_CREATED)
 * @param user_data Optional data which was passed in with the subscription
 */

/**
 * Initalizes the Marmot system
 *
 * If the initialization fails, the function returns #FALSE, otherwise #TRUE.
 */
gboolean
marmot_init (void)
{
	DBusError error;

	dbus_error_init (&error);

	if (have_init) {
		return TRUE;
	}

	if (!g_thread_supported ())
		g_thread_init (NULL);

	dbus_conn = dbus_bus_get (DBUS_BUS_SESSION, NULL);
	if (!dbus_conn) {
		md_debug (DEBUG_INFO, "Could not connect to the bus.  We are hosed.");
		return FALSE;
	}

	if (!dbus_bus_activate_service (dbus_conn,
					MARMOT_SERVICE_NAME, 0, NULL,
					&error)) {
		md_debug (DEBUG_INFO,
			  "Could not activate the Marmot service: %s",
			  error.message);
		dbus_error_free (&error);
		return FALSE;
	}
	
	g_thread_create (marmot_monitor_main_loop, NULL, TRUE, NULL);

	monitor_hash = g_hash_table_new (g_str_hash, g_str_equal);
	
	have_init = TRUE;

	return have_init;
}

/**
 * Creates a new Marmot.  This is simply a handle each client needs
 * to have in order to subscribe to file/directory events.
 *
 * @returns the new Marmot
 */
Marmot *
marmot_monitor_new (void)
{
	Marmot *monitor;
	const char* service_name;
       
	g_return_val_if_fail (have_init, NULL);
	
	G_LOCK(dbus_conn);
	service_name = dbus_bus_get_base_service (dbus_conn);
	G_UNLOCK(dbus_conn);

	monitor = g_new (Marmot, 1);
	monitor->listeners = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
			                            (GDestroyNotify) marmot_listener_free);
	monitor->id = g_strdup_printf ("%s-%d", service_name, num_listeners);
	monitor->lock = g_mutex_new ();

	G_LOCK(monitor_hash);
	g_hash_table_insert (monitor_hash, monitor->id, monitor);
	num_listeners++;
	G_UNLOCK(monitor_hash);

	return monitor;
}

/**
 * Destroys a previously created Marmot instance
 *
 * This will cancel all active subscriptions for the given monitor
 *
 * @param monitor the Marmot
 */
void
marmot_monitor_destroy (Marmot *monitor)
{
	g_return_if_fail (monitor != NULL);

	G_LOCK (monitor_hash);
	g_hash_table_remove (monitor_hash, monitor->id);
	G_UNLOCK (monitor_hash);

	g_hash_table_foreach (monitor->listeners,
			      (GHFunc)marmot_monitor_cancel_foreach, monitor);
	g_hash_table_destroy (monitor->listeners);
	g_mutex_free (monitor->lock);
	g_free (monitor->id);
	g_free (monitor);
}

/**
 * Registers to receive specific events for a given file.
 *
 * @param monitor a Marmot, created with #marmot_monitor_new
 * @param path The full path to the file to be monitored
 * @param events an ORed value of events you wish to receive, e.g.
 * MARMOT_EVENT_CHANGED|MARMOT_EVENT_DELETED
 * @param cb A callback which will be invoked whenever an event occurs
 * @param user_data Optional data which will be passed to the callback
 * @returns whether the subscription succeeded or not
 */
gboolean
marmot_subscribe_file (Marmot *monitor,
		         const char *path,
			 int events,
			 MarmotEventCallbackFn cb,
			 gpointer user_data)
{
	MarmotListener *listener;
	DBusMessage *msg;

	g_return_val_if_fail (have_init, FALSE);
	g_return_val_if_fail (path != NULL, FALSE);

	g_mutex_lock (monitor->lock);
	if (g_hash_table_lookup (monitor->listeners, path) != NULL) {
		g_mutex_unlock (monitor->lock);
		return FALSE;
	}

	g_mutex_unlock (monitor->lock);

	listener = marmot_listener_new (path, cb, user_data);

	msg = dbus_message_new (MARMOT_MESSAGE_SUBSCRIBE_FILE,
				MARMOT_SERVICE_NAME);
	dbus_message_append_args (msg, DBUS_TYPE_STRING, monitor->id,
				  DBUS_TYPE_STRING, path,
				  DBUS_TYPE_UINT32, events, 0);

	G_LOCK (dbus_conn);
	if (!dbus_connection_send (dbus_conn, msg, NULL)) {
		G_UNLOCK (dbus_conn);
		return FALSE;
	} else {
		md_debug (DEBUG_INFO,
			  "Sent message: %s",
			  MARMOT_MESSAGE_SUBSCRIBE_FILE);
	}

	G_UNLOCK (dbus_conn);

	g_mutex_lock (monitor->lock);
	g_hash_table_insert (monitor->listeners, listener->path, listener);
	g_mutex_unlock (monitor->lock);

	return TRUE;
}

/**
 * Registers to receive specific events for a given directory.
 *
 * @param monitor a Marmot, created with #marmot_monitor_new
 * @param path The full path to the directory to be monitored
 * @param events an ORed value of events you wish to receive, e.g.
 * MARMOT_EVENT_CHANGED|MARMOT_EVENT_DELETED
 * @param recursive Whether the directory should be monitored recursively
 * @param filter a regular expression which will be used to match against file
 * names.  e.g., ".*\.txt" would receive events only for files ending in ".txt"
 * @param cb A callback which will be invoked whenever an event occurs
 * @param user_data Optional data which will be passed to the callback
 * @returns whether the subscription succeeded or not
 */
gboolean
marmot_subscribe_directory (Marmot *monitor,
		              const char *path,
			      int events,
			      gboolean recursive,
			      const char *filter,
			      MarmotEventCallbackFn cb,
			      gpointer user_data)
{
	MarmotListener *listener;
	DBusMessage *msg;

	g_return_val_if_fail (have_init, FALSE);
	g_return_val_if_fail (path != NULL, FALSE);

	g_mutex_lock (monitor->lock);
	if (g_hash_table_lookup (monitor->listeners, path) != NULL) {
		g_mutex_unlock (monitor->lock);
		return FALSE;
	}

	g_mutex_unlock (monitor->lock);

	listener = marmot_listener_new (path, cb, user_data);

	msg = dbus_message_new (MARMOT_MESSAGE_SUBSCRIBE_DIRECTORY,
				MARMOT_SERVICE_NAME);

	dbus_message_append_args (msg, DBUS_TYPE_STRING, monitor->id,
				  DBUS_TYPE_STRING, path,
				  DBUS_TYPE_UINT32, events,
				  DBUS_TYPE_UINT32, recursive,
				  DBUS_TYPE_STRING, filter ? filter : "*", 0);

	G_LOCK (dbus_conn);
	if (!dbus_connection_send (dbus_conn, msg, NULL)) {
		G_UNLOCK (dbus_conn);
		return FALSE;
	} else {
		md_debug (DEBUG_INFO, "Sent message: %s", MARMOT_MESSAGE_SUBSCRIBE_DIRECTORY);
	}

	G_UNLOCK (dbus_conn);

	g_mutex_lock (monitor->lock);
	g_hash_table_insert (monitor->listeners, listener->path, listener);
	g_mutex_unlock (monitor->lock);

	return TRUE;
}


/**
 * Cancels a previously created subscription
 *
 * @param monitor a Marmot
 * @param path the path that was being monitored
 */
void
marmot_cancel (Marmot *monitor,
		 const char *path)
{
	g_return_if_fail (have_init == TRUE);
	g_return_if_fail (monitor != NULL);

	g_mutex_lock (monitor->lock);
	g_hash_table_remove (monitor->listeners, path);
	g_mutex_unlock (monitor->lock);

	marmot_monitor_send_cancel (monitor, path);
}


/** @} */
